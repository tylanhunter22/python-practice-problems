# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if len(values) == 0:
        return None
    maximum = values[0]
    for num in values:
        if num > maximum:
            maximum = num
    return maximum

print(max_in_list([1, 4, 5, 16]))
