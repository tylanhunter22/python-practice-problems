# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    num_attend = len(attendees_list)
    num_memb = len(members_list)
    if num_attend >= num_memb * 0.5:
        return "Half of members or over attended"
    else:
        return "Under half of our members attended, lets bring alcohol next time"


print(has_quorum(49, 100))
