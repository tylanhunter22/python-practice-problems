# Complete the minimum_value function so that returns the
# minimum of two values.
#
# If the values are the same, return either.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

# def minimum_value(value1, value2):
#     min = value1        #setting a minimum value variable
#     if value2 < value1: #compares value1 to value2, if 2 is less than 1
#         min = value2    #then min variable will then become value2
#     return min          #calls us out of the function

# print(minimum_value(5, 3))

# or

def minimum_value(value1, value2):
    if value1 < value2:             #if 1 less than 2
        return value1               #return1
    else:                           #if not, go with 2
        return value2

print(minimum_value(1, 2))
